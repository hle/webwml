msgid ""
msgstr ""
"Project-Id-Version: bugs 1.15\n"
"PO-Revision-Date: 2009-08-13 20:50+0200\n"
"Last-Translator: Miroslav Kure <kurem@debian.cz>\n"
"Language-Team: Czech <debian-l10n-czech@lists.debian.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/Bugs/pkgreport-opts.inc:17
msgid "in package"
msgstr "v balíčku"

#: ../../english/Bugs/pkgreport-opts.inc:20
#: ../../english/Bugs/pkgreport-opts.inc:60
#: ../../english/Bugs/pkgreport-opts.inc:94
msgid "tagged"
msgstr "se štítky"

#: ../../english/Bugs/pkgreport-opts.inc:23
#: ../../english/Bugs/pkgreport-opts.inc:63
#: ../../english/Bugs/pkgreport-opts.inc:97
msgid "with severity"
msgstr "se závažností"

#: ../../english/Bugs/pkgreport-opts.inc:26
msgid "in source package"
msgstr "ve zdrojovém balíčku"

#: ../../english/Bugs/pkgreport-opts.inc:29
msgid "in packages maintained by"
msgstr "v balíčku spravovaném"

#: ../../english/Bugs/pkgreport-opts.inc:32
msgid "submitted by"
msgstr "zaslané"

#: ../../english/Bugs/pkgreport-opts.inc:35
msgid "owned by"
msgstr "vlastněné"

#: ../../english/Bugs/pkgreport-opts.inc:38
msgid "with status"
msgstr "se stavem"

#: ../../english/Bugs/pkgreport-opts.inc:41
msgid "with mail from"
msgstr "obsahující e-mail od"

#: ../../english/Bugs/pkgreport-opts.inc:44
msgid "newest bugs"
msgstr "nejnovější chyby"

#: ../../english/Bugs/pkgreport-opts.inc:57
#: ../../english/Bugs/pkgreport-opts.inc:91
msgid "with subject containing"
msgstr "s předmětem obsahujícím"

#: ../../english/Bugs/pkgreport-opts.inc:66
#: ../../english/Bugs/pkgreport-opts.inc:100
msgid "with pending state"
msgstr "se stavem čekající"

#: ../../english/Bugs/pkgreport-opts.inc:69
#: ../../english/Bugs/pkgreport-opts.inc:103
msgid "with submitter containing"
msgstr "zasilatel obsahuje"

#: ../../english/Bugs/pkgreport-opts.inc:72
#: ../../english/Bugs/pkgreport-opts.inc:106
msgid "with forwarded containing"
msgstr "přeposláno obsahuje"

#: ../../english/Bugs/pkgreport-opts.inc:75
#: ../../english/Bugs/pkgreport-opts.inc:109
msgid "with owner containing"
msgstr "vlastník obsahuje"

#: ../../english/Bugs/pkgreport-opts.inc:78
#: ../../english/Bugs/pkgreport-opts.inc:112
msgid "with package"
msgstr "s balíčkem"

#: ../../english/Bugs/pkgreport-opts.inc:122
msgid "normal"
msgstr "normální"

#: ../../english/Bugs/pkgreport-opts.inc:125
msgid "oldview"
msgstr "původní"

#: ../../english/Bugs/pkgreport-opts.inc:128
msgid "raw"
msgstr "zdroj"

#: ../../english/Bugs/pkgreport-opts.inc:131
msgid "age"
msgstr "stáří"

#: ../../english/Bugs/pkgreport-opts.inc:137
msgid "Repeat Merged"
msgstr "Opakovat sloučené"

#: ../../english/Bugs/pkgreport-opts.inc:138
msgid "Reverse Bugs"
msgstr "Obrátit pořadí chyb"

#: ../../english/Bugs/pkgreport-opts.inc:139
msgid "Reverse Pending"
msgstr "Obrátit čekající"

#: ../../english/Bugs/pkgreport-opts.inc:140
msgid "Reverse Severity"
msgstr "Obrátit závažnost"

#: ../../english/Bugs/pkgreport-opts.inc:141
msgid "No Bugs which affect packages"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:143
msgid "None"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:144
msgid "testing"
msgstr "testovací"

#: ../../english/Bugs/pkgreport-opts.inc:145
msgid "oldstable"
msgstr "předchozí stabilní"

#: ../../english/Bugs/pkgreport-opts.inc:146
msgid "stable"
msgstr "stabilní"

#: ../../english/Bugs/pkgreport-opts.inc:147
msgid "experimental"
msgstr "experimentální"

#: ../../english/Bugs/pkgreport-opts.inc:148
msgid "unstable"
msgstr "nestabilní"

#: ../../english/Bugs/pkgreport-opts.inc:152
msgid "Unarchived"
msgstr "Nearchivované"

#: ../../english/Bugs/pkgreport-opts.inc:155
msgid "Archived"
msgstr "Archivované"

#: ../../english/Bugs/pkgreport-opts.inc:158
msgid "Archived and Unarchived"
msgstr "Archivované i nearchivované"

#~ msgid "Exclude tag:"
#~ msgstr "Vynechat značku:"

#~ msgid "Include tag:"
#~ msgstr "Zahrnout značku:"

#~ msgid "lfs"
#~ msgstr "lfs"

#~ msgid "ipv6"
#~ msgstr "ipv6"

#~ msgid "wontfix"
#~ msgstr "nebudu opravovat"

#~ msgid "upstream"
#~ msgstr "původní autor"

#~ msgid "unreproducible"
#~ msgstr "nereprodukovatelné"

#~ msgid "security"
#~ msgstr "bezpečnost"

#~ msgid "patch"
#~ msgstr "patch"

#~ msgid "moreinfo"
#~ msgstr "více informací"

#~ msgid "l10n"
#~ msgstr "l10n"

#~ msgid "help"
#~ msgstr "pomoc"

#~ msgid "fixed-upstream"
#~ msgstr "opraveno u autora"

#~ msgid "fixed-in-experimental"
#~ msgstr "opraveno v experimental"

#~ msgid "d-i"
#~ msgstr "d-i"

#~ msgid "confirmed"
#~ msgstr "potvrzeno"

#~ msgid "sid"
#~ msgstr "sid"

#~ msgid "etch-ignore"
#~ msgstr "etch-ignore"

#~ msgid "etch"
#~ msgstr "etch"

#~ msgid "sarge-ignore"
#~ msgstr "sarge-ignore"

#~ msgid "woody"
#~ msgstr "woody"

#~ msgid "potato"
#~ msgstr "potato"

#~ msgid "Exclude severity:"
#~ msgstr "Vynechat závažnost:"

#~ msgid "Include severity:"
#~ msgstr "Zahrnout závažnost:"

#~ msgid "wishlist"
#~ msgstr "přání"

#~ msgid "minor"
#~ msgstr "podružný"

#~ msgid "important"
#~ msgstr "důležitý"

#~ msgid "serious"
#~ msgstr "vážný"

#~ msgid "grave"
#~ msgstr "závažný"

#~ msgid "critical"
#~ msgstr "kritický"

#~ msgid "Exclude status:"
#~ msgstr "Vynechat stav:"

#~ msgid "Include status:"
#~ msgstr "Zahrnout stav:"

#~ msgid "done"
#~ msgstr "vyřešený"

#~ msgid "fixed"
#~ msgstr "opravený"

#~ msgid "pending"
#~ msgstr "čekající"

#~ msgid "forwarded"
#~ msgstr "přeposlaný"

#~ msgid "open"
#~ msgstr "otevřený"

#~ msgid "bugs"
#~ msgstr "chyby"

#~ msgid "Distribution:"
#~ msgstr "Distribuce:"

#~ msgid "Package version:"
#~ msgstr "Verze balíčku:"

#~ msgid "testing-proposed-updates"
#~ msgstr "navrhované testovací aktualizace"

#~ msgid "proposed-updates"
#~ msgstr "navrhované aktualizace"

#~ msgid "don't show statistics in the footer"
#~ msgstr "v patičce nezobrazovat statistiku"

#~ msgid "don't show table of contents in the header"
#~ msgstr "v záhlaví nezobrazovat obsah"

#~ msgid "no ordering by status or severity"
#~ msgstr "netřídit podle stavu nebo závažnosti"

#~ msgid "display merged bugs only once"
#~ msgstr "zobrazit sloučené chyby pouze jednou"

#~ msgid "active bugs"
#~ msgstr "aktivní chyby"

#~ msgid "Flags:"
#~ msgstr "Příznaky:"
