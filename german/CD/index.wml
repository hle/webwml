#use wml::debian::cdimage title="Debian auf CD/DVD" BARETITLE=true
#use wml::debian::release_info
#use wml::debian::translation-check translation="9dcab36202d262b6b33583d68794b995999fa269"
# $Id$
# Translator: Gerfried Fuchs <rhonda@debian.org> 2002-01-18
# Updated: Holger Wansing <linux@wansing-online.de>, 2011, 2015, 2018.

<p>Wenn Sie Debian auf CD/DVD oder USB-Stick haben wollen, finden Sie
hier Informationen über die verfügbaren Möglichkeiten.
Falls Sie Probleme dabei haben, schauen Sie bitte in
die <a href="faq/">FAQ über Debian-CDs/DVDs</a>.</p>

<div class="tip">
<p>Wenn Sie Debian einfach installieren wollen und eine Internetverbindung
auf dem Zielrechner möglich ist, ziehen Sie bitte die
<a href="netinst/">Netzinstallation</a> in Betracht, die eine geringere
Größe zum Herunterladen benötigt.</p>
</div>

<div class="tip">
<p>Auf i386- und amd64-Architekturen können alle CD- und DVD-Images
auch auf
<a href="https://www.debian.org/CD/faq/#write-usb">USB-Sticks
verwendet werden</a>.</div>

<ul>
  <li><a href="http-ftp/">CD-/DVD-Images über HTTP herunterladen.</a>
  Viele Spiegel bieten direkte HTTP-Links zum Herunterladen an, auf
  die Sie mit Ihrem Browser oder Kommandozeilenwerkzeug zugreifen können.</li>

  <li><a href="vendors/">Kaufen von fertigen Debian-Installationsmedien.</a> Sie sind
  billig &ndash; wir machen damit keinen Gewinn! Falls Ihre Internet-Anbindung nach
  Minuten abgerechnet wird, ist das Ihre einzige Wahl. Sie können es auch in
  Erwägung ziehen, solche Medien zu kaufen, wenn Sie nur eine langsame Internet-Verbindung
  haben, da das Herunterladen all der Images ziemlich lange dauern kann.</li>

  <li><a href="jigdo-cd/">CD-/DVD-Images mit jigdo herunterladen.</a> Das
  <q>jigdo</q>-Schema erlaubt es Ihnen, den schnellsten der weltweit 300
  Debian-Spiegel zum Herunterladen zu verwenden. Es bietet einfache
  Spiegel-Auswahl und <q>Aktualisierung</q> von älteren Images auf die letzte
  Veröffentlichung. Außerdem ist es die einzige Möglichkeit, Debian-DVD-Images
  für <em>alle</em> Architekturen herunterzuladen.</li>

  <li><a href="torrent-cd/">CD-/DVD-Images mit BitTorrent herunterladen.</a>
  Das BitTorrent Peer-to-peer-System ermöglicht vielen Benutzern
  gleichzeitig, beim Herunterladen von Images zusammenzuarbeiten.
  Dabei entsteht nur eine minimale Auslastung unserer Server.
  DVD-Images sind nur für einige Architekturen verfügbar.</li>

  <li><a href="live/">Laden Sie Live-Images über HTTP, FTP oder BitTorrent
  herunter.</a> Ebenfalls als neue Alternative zu den Standard-Images gibt
  es Live-Images, die Sie dazu verwenden können, Debian zuerst auszuprobieren
  und dann den Inhalt des Images zu installieren.</li>

</ul>

<p>Offizielle CD-/DVD-Veröffentlichungen sind signiert, so dass Sie <a
href="verify">verifizieren können, ob sie authentisch sind</a>.</p>

<p>Debian ist für verschiedene Computer-Architekturen verfügbar &ndash; vergewissern
Sie sich, dass Sie sich passende Images für Ihren Computer besorgen! (Die meisten
Leute benötigen Images für <q>amd64</q>, also 64-Bit PC-kompatible Systeme.)
Wenn Sie Ihre eigenen Disks erstellt haben, könnten Sie an
<a href="artwork/">Artwork für Hüllen von Debian-Disks</a> interessiert
sein.</p>

# Letztes Release: webwml/english/template/debian/cdimage.wml ändern
      <div class="cdflash" id="latest">Neueste offizielle Veröffentlichung der
      <q>Stable</q>-CD-/DVD-Images:
        <strong><current-cd-release></strong>.
      <br /><small>(Schnappschüsse der <q>Testing</q>-Distribution werden
      wöchentlich erstellt.)</small></div>

# <release-notes> wird in releases/index.wml verwendet, setzt die Variablen
{#releases#:
#include "releases/index.wml"
:##}

<p>
  Informationen zu bekannten Installationsproblemen finden Sie auf der <a
  href="$(HOME)/releases/stable/debian-installer/">Installationsinformationsseite</a>.<br />
</p>
