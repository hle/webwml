msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 16:56+0200\n"
"Last-Translator: Josip Rodin\n"
"Language-Team: Croatian\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr ""

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr ""

#. One male delegate
#: ../../english/intro/organization.data:18
msgid "<void id=\"male\"/>delegate"
msgstr ""

#. One female delegate
#: ../../english/intro/organization.data:20
msgid "<void id=\"female\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:25
msgid "current"
msgstr "trenutni"

#: ../../english/intro/organization.data:27
#: ../../english/intro/organization.data:29
msgid "member"
msgstr "član"

#: ../../english/intro/organization.data:32
msgid "manager"
msgstr "upravitelj"

#: ../../english/intro/organization.data:34
msgid "SRM"
msgstr ""

#: ../../english/intro/organization.data:34
#, fuzzy
#| msgid "Release Management"
msgid "Stable Release Manager"
msgstr "Nadgledanje izdanja"

#: ../../english/intro/organization.data:36
msgid "wizard"
msgstr "čarobnjak"

#: ../../english/intro/organization.data:39
#, fuzzy
#| msgid "chairman"
msgid "chair"
msgstr "predsjedavatelj"

#: ../../english/intro/organization.data:42
msgid "assistant"
msgstr "pomoćnik"

#: ../../english/intro/organization.data:44
msgid "secretary"
msgstr "tajnik"

#: ../../english/intro/organization.data:53
#: ../../english/intro/organization.data:66
msgid "Officers"
msgstr "Službenici"

#: ../../english/intro/organization.data:54
#: ../../english/intro/organization.data:90
msgid "Distribution"
msgstr "Distribucija"

#: ../../english/intro/organization.data:55
#: ../../english/intro/organization.data:232
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:57
#: ../../english/intro/organization.data:235
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:58
#: ../../english/intro/organization.data:239
#, fuzzy
#| msgid "Publicity"
msgid "Publicity team"
msgstr "Publicitet"

#: ../../english/intro/organization.data:60
#: ../../english/intro/organization.data:304
msgid "Support and Infrastructure"
msgstr "Podrška i infrastruktura"

#. formerly Custom Debian Distributions (CCDs); see https://blends.debian.org/blends/ch-about.en.html#s-Blends
#: ../../english/intro/organization.data:62
msgid "Debian Pure Blends"
msgstr ""

#: ../../english/intro/organization.data:69
msgid "Leader"
msgstr "Vođa"

#: ../../english/intro/organization.data:71
msgid "Technical Committee"
msgstr "Tehnički odbor"

#: ../../english/intro/organization.data:85
msgid "Secretary"
msgstr "Tajnik"

#: ../../english/intro/organization.data:93
msgid "Development Projects"
msgstr "Razvojni projekti"

#: ../../english/intro/organization.data:94
msgid "FTP Archives"
msgstr "FTP arhive"

#: ../../english/intro/organization.data:96
#, fuzzy
#| msgid "FTP Master"
msgid "FTP Masters"
msgstr "FTP Master"

#: ../../english/intro/organization.data:102
msgid "FTP Assistants"
msgstr "FTP Assistants"

#: ../../english/intro/organization.data:107
msgid "FTP Wizards"
msgstr ""

#: ../../english/intro/organization.data:111
msgid "Backports"
msgstr ""

#: ../../english/intro/organization.data:113
msgid "Backports Team"
msgstr ""

#: ../../english/intro/organization.data:117
msgid "Individual Packages"
msgstr "Pojedini paketi"

#: ../../english/intro/organization.data:118
msgid "Release Management"
msgstr "Nadgledanje izdanja"

#: ../../english/intro/organization.data:120
msgid "Release Team"
msgstr "Tim za izdanja"

#: ../../english/intro/organization.data:133
msgid "Quality Assurance"
msgstr "Osiguranje kvalitete"

#: ../../english/intro/organization.data:134
msgid "Installation System Team"
msgstr "Tim za instalacijski sustav"

#: ../../english/intro/organization.data:135
msgid "Release Notes"
msgstr "Napomene izdanja"

#: ../../english/intro/organization.data:137
msgid "CD Images"
msgstr "CD snimke"

#: ../../english/intro/organization.data:139
msgid "Production"
msgstr "Izrada"

#: ../../english/intro/organization.data:147
msgid "Testing"
msgstr "Testiranje"

#: ../../english/intro/organization.data:149
#, fuzzy
#| msgid "Support and Infrastructure"
msgid "Autobuilding infrastructure"
msgstr "Podrška i infrastruktura"

#: ../../english/intro/organization.data:151
msgid "Wanna-build team"
msgstr ""

#: ../../english/intro/organization.data:159
#, fuzzy
#| msgid "Buildd Administration"
msgid "Buildd administration"
msgstr "Administracija buildda"

#: ../../english/intro/organization.data:178
msgid "Documentation"
msgstr "Dokumentacija"

#: ../../english/intro/organization.data:183
msgid "Work-Needing and Prospective Packages list"
msgstr "Popis paketa koji zahtijevaju rad i paketi u prospektu"

#: ../../english/intro/organization.data:186
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:187
msgid "Ports"
msgstr "Portovi"

#: ../../english/intro/organization.data:222
msgid "Special Configurations"
msgstr "Posebne konfiguracije"

#: ../../english/intro/organization.data:225
msgid "Laptops"
msgstr "Laptopi"

#: ../../english/intro/organization.data:226
msgid "Firewalls"
msgstr "Firewalli"

#: ../../english/intro/organization.data:227
msgid "Embedded systems"
msgstr "<i>Embedded</i> sustavi"

#: ../../english/intro/organization.data:242
msgid "Press Contact"
msgstr "Kontakt za tisak"

#: ../../english/intro/organization.data:244
msgid "Web Pages"
msgstr "WWW stranice"

#: ../../english/intro/organization.data:254
msgid "Planet Debian"
msgstr "Planet Debian"

#: ../../english/intro/organization.data:259
msgid "Outreach"
msgstr ""

#: ../../english/intro/organization.data:263
msgid "Debian Women Project"
msgstr "Debian Women projekt"

#: ../../english/intro/organization.data:271
msgid "Anti-harassment"
msgstr ""

#: ../../english/intro/organization.data:276
msgid "Events"
msgstr "Događaji"

#: ../../english/intro/organization.data:282
#, fuzzy
#| msgid "Technical Committee"
msgid "DebConf Committee"
msgstr "Tehnički odbor"

#: ../../english/intro/organization.data:289
msgid "Partner Program"
msgstr "Program partnera"

#: ../../english/intro/organization.data:294
msgid "Hardware Donations Coordination"
msgstr "Koordinacija donacija hardvera"

#: ../../english/intro/organization.data:307
msgid "User support"
msgstr "Podrška korisnicima"

#: ../../english/intro/organization.data:374
msgid "Bug Tracking System"
msgstr "Sustav praćenja bugova"

#: ../../english/intro/organization.data:379
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Administracija mailing lista i arhiva mailing lista"

#: ../../english/intro/organization.data:387
#, fuzzy
#| msgid "New Maintainers Front Desk"
msgid "New Members Front Desk"
msgstr "Front-desk za nove održavatelje"

#: ../../english/intro/organization.data:393
msgid "Debian Account Managers"
msgstr "Upravitelji korisničkih računa razvijatelja"

#: ../../english/intro/organization.data:397
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:398
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Održavatelji prstena ključeva (PGP i GPG)"

#: ../../english/intro/organization.data:402
msgid "Security Team"
msgstr "Sigurnosni tim"

#: ../../english/intro/organization.data:414
msgid "Consultants Page"
msgstr "Stranica za konzultante"

#: ../../english/intro/organization.data:419
msgid "CD Vendors Page"
msgstr "Stranica za distributere CD-a"

#: ../../english/intro/organization.data:422
msgid "Policy"
msgstr "Policy"

#: ../../english/intro/organization.data:425
msgid "System Administration"
msgstr "Administracija sustava"

#: ../../english/intro/organization.data:426
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Ovo je adresa koju treba koristiti kada imate problema na jednom od "
"Debianovih strojeva, uključujući probleme sa lozinkama i potrebe za "
"instaliranjem paketa."

#: ../../english/intro/organization.data:435
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Ako imate hardverskih problema s Debian strojevima, molimo pogledajte "
"stranicu <a href=\"https://db.debian.org/machines.cgi\">Debian strojeva</a>, "
"koja bi trebala sadržavati informacije o administratorima pojedinih strojeva."

#: ../../english/intro/organization.data:436
msgid "LDAP Developer Directory Administrator"
msgstr "Administrator LDAP direktorija razvijatelja"

#: ../../english/intro/organization.data:437
msgid "Mirrors"
msgstr "Mirrori"

#: ../../english/intro/organization.data:444
msgid "DNS Maintainer"
msgstr "Održavatelj DNS-a"

#: ../../english/intro/organization.data:445
msgid "Package Tracking System"
msgstr "Sustav praćenja paketa"

#: ../../english/intro/organization.data:447
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:453
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""

#: ../../english/intro/organization.data:456
#, fuzzy
#| msgid "Alioth administrators"
msgid "Salsa administrators"
msgstr "Administratori Aliotha"

#: ../../english/intro/organization.data:467
msgid "Debian for children from 1 to 99"
msgstr "Debian za djecu od 1 do 99"

#: ../../english/intro/organization.data:470
msgid "Debian for medical practice and research"
msgstr "Debian za medicinsku praksu i istraživanje"

#: ../../english/intro/organization.data:473
msgid "Debian for education"
msgstr "Debian za obrazovanje"

#: ../../english/intro/organization.data:478
msgid "Debian in legal offices"
msgstr "Debian u pravnim uredima"

#: ../../english/intro/organization.data:482
msgid "Debian for people with disabilities"
msgstr "Debian za ljude sa invaliditetom"

#: ../../english/intro/organization.data:486
#, fuzzy
#| msgid "Debian for medical practice and research"
msgid "Debian for science and related research"
msgstr "Debian za medicinsku praksu i istraživanje"

#: ../../english/intro/organization.data:489
#, fuzzy
#| msgid "Debian for education"
msgid "Debian for astronomy"
msgstr "Debian za obrazovanje"

#~ msgid "Testing Security Team"
#~ msgstr "Sigurnosni tim za <q>testing</q>"

#~ msgid "Security Audit Project"
#~ msgstr "Projekt Security Audit"

#~ msgid ""
#~ "This is not yet an official Debian internal project but it has announced "
#~ "the intention to be integrated."
#~ msgstr ""
#~ "Ovo još nije službeni interni projekt u Debianu ali su najavili namjeru "
#~ "integracije."

#~ msgid "Debian for non-profit organisations"
#~ msgstr "Debian za neprofitne organizacije"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "Univerzalni operativni sustav kao vaš desktop"

#~ msgid "Accountant"
#~ msgstr "Knjigovođa"

#~ msgid "Key Signing Coordination"
#~ msgstr "Koordinacija potpisivanja ključeva"

#~ msgid ""
#~ "Names of individual buildd's admins can also be found on <a href=\"http://"
#~ "www.buildd.net\">http://www.buildd.net</a>.  Choose an architecture and a "
#~ "distribution to see the available buildd's and their admins."
#~ msgstr ""
#~ "Imena pojedinih administratora builddova se mogu naći i na <a href="
#~ "\"http://www.buildd.net\">http://www.buildd.net</a>. Odaberite "
#~ "arhitekturu i distribuciju kako bi vidjeli dostupne builddove i njihove "
#~ "administratore."

#~ msgid ""
#~ "The admins responsible for buildd's for a particular arch can be reached "
#~ "at <genericemail arch@buildd.debian.org>, for example <genericemail "
#~ "i386@buildd.debian.org>."
#~ msgstr ""
#~ "Administratori koji su odgovorni za builddove za pojedinu arhitekturu se "
#~ "mogu dobiti na <genericemail arch@buildd.debian.org>, npr. <genericemail "
#~ "i386@buildd.debian.org>."

#~ msgid "Marketing Team"
#~ msgstr "Marketing tim"

#~ msgid "Handhelds"
#~ msgstr "Ručna računala (dlanovnici)"

#~ msgid "APT Team"
#~ msgstr "APT tim"

#~ msgid "Vendors"
#~ msgstr "Distributeri"

#~ msgid "Release Team for ``stable''"
#~ msgstr "Tim za izdanje <q>stable</q>"

#~ msgid "Custom Debian Distributions"
#~ msgstr "Prilagođene Debian distribucije"

#, fuzzy
#~| msgid "Debian Maintainer Keyring Team"
#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Debian Maintainer keyring tim"

#~ msgid "Publicity"
#~ msgstr "Publicitet"

#~ msgid "Auditor"
#~ msgstr "Revizor"

#, fuzzy
#~| msgid "Installation System Team"
#~ msgid "Live System Team"
#~ msgstr "Tim za instalacijski sustav"

#~ msgid "Alioth administrators"
#~ msgstr "Administratori Aliotha"
