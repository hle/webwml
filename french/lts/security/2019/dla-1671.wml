#use wml::debian::translation-check translation="244380ed6414a14f265795cff6ac8dab1d04e3a3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans coTURN, un serveur TURN et
STUN pour VoIP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-4056">CVE-2018-4056</a>

<p>Une vulnérabilité d’injection SQL a été découverte dans le portail web de
l’administrateur de coTURN. Comme l’interface web d’administration est partagée
avec la production, il n’est malheureusement pas possible de filtrer aisément
les accès extérieurs et cette mise à jour de sécurité désactive complètement
l’interface web. Les utilisateurs devraient utiliser à la place l’interface en
ligne de commande.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-4058">CVE-2018-4058</a>

<p>La configuration par défaut autorise la retransmission non sécurisée sur
boucle locale. Un attaquant distant ayant accès à l’interface TURN peut utiliser
cette vulnérabilité pour obtenir l’accès à des services qui ne devraient être
que locaux.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-4059">CVE-2018-4059</a>

<p>La configuration par défaut utilise un mot de passe vide pour l’interface
d’administration locale en ligne de commande. Un attaquant avec accès à la
console locale (soit un attaquant local ou un attaquant distant tirant parti de
<a href="https://security-tracker.debian.org/tracker/CVE-2018-4058">CVE-2018-4058</a>)
pourrait augmenter ses privilèges à ceux de l’administrateur de serveur coTURN.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 4.2.1.2-1+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets coturn.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1671.data"
# $Id: $
