<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in the Tomcat
servlet and JSP engine.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7674">CVE-2017-7674</a>

    <p>The CORS Filter in Apache Tomcat did not add an HTTP Vary header
    indicating that the response varies depending on Origin. This
    permitted client and server side cache poisoning in some
    circumstances.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12616">CVE-2017-12616</a>

    <p>When using a VirtualDirContext with Apache Tomcat it was possible to
    bypass security constraints and/or view the source code of JSPs for
    resources served by the VirtualDirContext using a specially crafted
    request.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1304">CVE-2018-1304</a>

    <p>The URL pattern of "" (the empty string) which exactly maps to the
    context root was not correctly handled in Apache Tomcat when used as
    part of a security constraint definition. This caused the constraint
    to be ignored. It was, therefore, possible for unauthorized users to
    gain access to web application resources that should have been
    protected. Only security constraints with a URL pattern of the empty
    string were affected.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1305">CVE-2018-1305</a>

    <p>Security constraints defined by annotations of Servlets in Apache
    Tomcat were only applied once a Servlet had been loaded. Because
    security constraints defined in this way apply to the URL pattern
    and any URLs below that point, it was possible - depending on the
    order Servlets were loaded - for some security constraints not to be
    applied. This could have exposed resources to users who were not
    authorized to access them.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8014">CVE-2018-8014</a>

    <p>The defaults settings for the CORS filter provided in Apache Tomcat
    are insecure and enable <q>supportsCredentials</q> for all origins. It is
    expected that users of the CORS filter will have configured it
    appropriately for their environment rather than using it in the
    default configuration. Therefore, it is expected that most users
    will not be impacted by this issue.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
7.0.56-3+really7.0.88-1.</p>

<p>We recommend that you upgrade your tomcat7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1400.data"
# $Id: $
