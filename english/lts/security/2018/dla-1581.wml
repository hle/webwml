<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Multiple vulnerabilities have been discovered in uriparser, an Uniform Resource
Identifiers (URIs) parsing library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19198">CVE-2018-19198</a>

    <p>UriQuery.c allows an out-of-bounds write via a uriComposeQuery* or
    uriComposeQueryEx* function because the '&' character is mishandled in
    certain contexts.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19199">CVE-2018-19199</a>

    <p>UriQuery.c allows an integer overflow via a uriComposeQuery* or
    uriComposeQueryEx* function because of an unchecked multiplication.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19200">CVE-2018-19200</a>

    <p>UriCommon.c allows attempted operations on NULL input via a uriResetUri*
    function.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.8.0.1-2+deb8u1.</p>

<p>We recommend that you upgrade your uriparser packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1581.data"
# $Id: $
