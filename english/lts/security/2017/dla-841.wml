<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This upload fixes a security vulnerability in the header parsing code.</p>

<p>David Dennerline, of IBM Security's X-Force Researchers, and R�gis
Leroy discovered problems in the way Apache handled a broad pattern of
unusual whitespace patterns in HTTP requests. In some configurations,
this could lead to response splitting or cache pollution
vulnerabilities. To fix these issues, this update makes Apache httpd
be more strict in what HTTP requests it accepts.</p>

<p>If this causes problems with non-conforming clients, some checks can
be relaxed by adding the new directive <q>HttpProtocolOptions unsafe</q> to
the configuration.  More information is available at</p>

<p><url "http://httpd.apache.org/docs/2.4/mod/core.html#httpprotocoloptions"></p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.2.22-13+deb7u8.</p>

<p>We recommend that you upgrade your apache2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-841.data"
# $Id: $
