<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The fix for <a href="https://security-tracker.debian.org/tracker/CVE-2017-14990">CVE-2017-14990</a> issued as DLA-1151-1 was incomplete and
caused a regression. It was discovered that an additional database
upgrade and further code changes would be necessary. At the moment
these changes are deemed as too intrusive and thus the initial patch
for <a href="https://security-tracker.debian.org/tracker/CVE-2017-14990">CVE-2017-14990</a> has been removed again. For reference, the original
advisory text follows.</p>

<p>WordPress stores cleartext wp_signups.activation_key values (but
stores the analogous wp_users.user_activation_key values as hashes),
which might make it easier for remote attackers to hijack unactivated
user accounts by leveraging database read access (such as access
gained through an unspecified SQL injection vulnerability).</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.6.1+dfsg-1~deb7u19.</p>

<p>We recommend that you upgrade your wordpress packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1151-2.data"
# $Id: $
