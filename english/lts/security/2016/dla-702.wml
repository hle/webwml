<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update includes the changes in tzdata 2016i. Notable
changes are:</p>

<ul>
<li>Pacific/Tongatapu (DST starting on 2016-11-06 at 02:00).</li>
<li>Northern Cyprus is now +03 year round, the Asia/Famagusta zone has
   been added.</li>
<li>Antarctica/Casey (switched from +08 to +11 on 2016-10-22).</li>
</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2016i-0+deb7u1.</p>

<p>We recommend that you upgrade your tzdata packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-702.data"
# $Id: $
