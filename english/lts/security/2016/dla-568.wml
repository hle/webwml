<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities were discovered in wordpress, a web blogging
tool. The Common Vulnerabilities and Exposures project identifies the
following issues.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5387">CVE-2016-5387</a>

    <p>WordPress allows remote attackers to bypass intended
    access restrictions and remove a category attribute from a post via
    unspecified vectors.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5832">CVE-2016-5832</a>

    <p>The customizer in WordPress allows remote attackers to
    bypass intended redirection restrictions via unspecified vectors.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5834">CVE-2016-5834</a>

    <p>Cross-site scripting (XSS) vulnerability in the
    wp_get_attachment_link function in wp-includes/post
    template.php in WordPress allows remote
    attackers to inject arbitrary web script or HTML via a crafted
    attachment name.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5835">CVE-2016-5835</a>

    <p>WordPress allows remote attackers to obtain sensitive
    revision-history information by leveraging the ability to read a
    post related to wp-admin/includes/ajax-actions.php and
    wp-admin/revision.php.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5838">CVE-2016-5838</a>

    <p>WordPress allows remote attackers to bypass intended password
    change restrictions by leveraging knowledge of a cookie.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5839">CVE-2016-5839</a>

    <p>WordPress allows remote attackers to bypass the
    sanitize_file_name protection mechanism via unspecified vectors.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.6.1+dfsg-1~deb7u11.</p>

<p>We recommend that you upgrade your wordpress packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-568.data"
# $Id: $
