<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Nagios was found to be vulnerable to two security issues that, when
combined, lead to a remote root code execution vulnerability.
Fortunately, the hardened permissions of the Debian package limit the
effect of those to information disclosure, but privilege escalation to
root is still possible locally.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9565">CVE-2016-9565</a>

    <p>Improper sanitization of RSS feed input enables unauthenticated
    remote read and write of arbitrary files which may lead to remote
    code execution if the web root is writable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9566">CVE-2016-9566</a>

    <p>Unsafe logfile handling allows unprivileged users to escalate their
    privileges to root. In wheezy, this is possible only through the
    debug logfile which is disabled by default.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.4.1-3+deb7u3.</p>

<p>We recommend that you upgrade your nagios3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-751.data"
# $Id: $
