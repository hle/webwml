<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This security update fixes a security issue in
ruby-mail. We recommend you upgrade your ruby-mail package.</p>

 <p>Takeshi Terada (Mitsui Bussan Secure Directions, Inc.) released a
 whitepaper entitled <q>SMTP Injection via recipient email addresses</q> 
 (<a href="http://www.mbsd.jp/Whitepaper/smtpi.pdf">http://www.mbsd.jp/Whitepaper/smtpi.pdf</a>). 
 This whitepaper has a section
 discussing how one such vulnerability affected the <q>mail</q> ruby gem (see
 section 3.1).</p>

 <p>Whitepaper has all the specific details, but basically the <q>mail</q> ruby gem
 module is prone to the recipient attack as it does not validate nor
 sanitize given recipient addresses. Thus, the attacks described in chapter
 2 of the whitepaper can be applied to the gem without any modification. The
 <q>mail</q> ruby gem itself does not impose a length limit on email addresses,
 so an attacker can send a long spam message via a recipient address unless
 there is a limit on the application's side. This vulnerability affects only
 the applications that lack input validation.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.4.4-2+deb7u1.</p>

<p>Further information about Debian LTS security Advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-489.data"
# $Id: $
